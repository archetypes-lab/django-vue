# django-vue

### Configuración Ambiente Desarrollo (DevContainer VSCODE)
* Instalar ultima versión de VSCODE
* Instalar Addons de Remote - Containers
* Clonar proyecto desde el GIT y abrir carpeta
* Abrir proyecto como contenedor (abrir como container folder, en el icono de extremo izquierda abajo)
* Esperar que se instale el ambiente de desarrollo
* Copiar (importante: es copiar no mover) archivo de configuración de ambiente src/envs/local.env.example a src/envs/local.env y configurar segun ambiente local.
* Lanzar proyecto con debug de visual studio code con el icono play: Python:Django


### Comandos Utiles
* crear nuevo modulo:  python manage.py startapp modulo
* crear migraciones: python manage.py makemigrations
* ejecutar migraciones: python manage.py migrate
