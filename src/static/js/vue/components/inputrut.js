Vue.component("inputrut", {
    components: {
        'pinputtext': inputtext
    },
    template:
        `
          <pinputtext
            type="text"
            placeholder="Ej. 12345678-9"
            ref="input"
            v-bind:value="value"
            v-on:input="updateValue($event)"
            v-on:focus="selectAll"
            v-on:blur="formatValue"
          >
      `,
    props: {
        value: {
            type: Number,
            default: 0
        }
    },
    mounted: function () {
        this.formatValue();
    },
    methods: {
        updateValue: function (value) {
            this.$emit("input", RutConverter.toNumber(value));
        },
        formatValue: function () {
            this.$refs.input.value = RutConverter.toString(this.value);
        },
        selectAll: function (event) {
            // Workaround for Safari bug
            // http://stackoverflow.com/questions/1269722/selecting-text-on-focus-using-jquery-not-working-in-safari-and-chrome
            setTimeout(function () {
                event.target.select();
            }, 0);
        }
    }
});