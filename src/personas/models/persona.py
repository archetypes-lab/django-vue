from django.db import models

class Persona(models.Model):
    rut = models.IntegerField(unique=True)
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)

    def __str__(self):
        return "{0} {1}".format(self.nombres, self.apellidos)
    