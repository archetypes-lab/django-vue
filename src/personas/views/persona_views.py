from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from django.core import serializers

from django.forms.models import model_to_dict

from personas.models.persona import Persona

class PersonaIndex(View):
    def get(self, request):
        personas = list(Persona.objects.all().values(
            'id', 'rut', 'nombres', 'apellidos'))
        context = {'personas': personas}
        return render(request, 'personas/index.html', context)

class PersonaEdit(View):
    def get(self, request, id_persona):
        queryset = Persona.objects.get(id=id_persona)
        context = {'persona': model_to_dict(queryset)}
        return render(request, 'personas/edit.html', context)

class GetPersonaJson(View):
    def get(self, request, id_persona):
        queryset = Persona.objects.get(id=id_persona)
        json = serializers.serialize('json', [queryset])
        return JsonResponse(json, safe=False)
