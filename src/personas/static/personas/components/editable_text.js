Vue.component('editable-text', {
    components: {
        'pinputtext': inputtext
    },
    data: () => {
        return {
          message: "Change me"
        }
    },
    delimiters: ["[[","]]"],
    template: 
    `
    <div><p>Message is: [[message]]</p>
    <pinputtext v-model="message" placeholder="edit me" /></div>
    `
});