from django.urls import path

from personas.views import PersonaIndex
from personas.views import PersonaEdit
from personas.views import GetPersonaJson

urlpatterns = [
    path('', PersonaIndex.as_view(), name='persona-index'),
    path('<int:id_persona>/', PersonaEdit.as_view(), name="persona-edit"),
    path('<int:id_persona>/json', GetPersonaJson.as_view(), name="persona-json")
]
